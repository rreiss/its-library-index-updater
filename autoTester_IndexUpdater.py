#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 2/26/18


import json
import requests

from Tools.util_udfTools import udfTools




# this will...
# update libraries that are are ready for pooling.  It will change the index of the library.  User will input (via file)
# library names to be updated with new indexes (index names are in another file)
# The purpose of this script is to prepare many libraries for pooling with new indexes.


class AutoTester_IndexUpdater():
    def __init__(self):
        self.printOn = False
        self.errorCnt = 0
        self.myUDF = udfTools()

        self.serverName = 'dev'
        self.server = 'claritydev1.jgi-psf.org'
        self.apiDevServer = 'clarity-dev01.jgi-psf.org'
        self.apiPrdServer = 'clarity-prd01.jgi-psf.org'
        self.indexIter = 0
        self.libraries = []   # list of libraries to be updated
        self.indexes = []   # list of indexes to use to update libraries



    # -------------------------------------------------------------------------------------------
    # getInput  -  need to get the libraries to be updated (file #1) with the indexes (file #2)
    # output:
    def getInput(self):

        #print ("getting library names from C:\clarity_jar\libraryNames.txt")
        libraryNamesFile = 'C:\clarity_jar\libraryNames.txt'
        f = open(libraryNamesFile, 'r')
        for line in f:  # expect only one flowcell id to retrieve
            self.libraries.append(line.strip())# get the second word,

        f.close()
        #now get the indexes that will needed to update the libraries, so every one is unique.

        indexNamesFile = 'C:\clarity_jar\indexes.txt'
        f = open(indexNamesFile, 'r')
        for line in f:  # expect only one flowcell id to retrieve
            self.indexes.append(line.strip())  # get the second word,
        f.close()
        if (len(self.libraries) > len(self.indexes)):
            print ("\n*** ERROR - The number of libraries (",len(self.libraries), ") is greater than the number of indexes(",len(self.indexes),")!\nThere must be at least the same "
                   "number of indexes in the indexes.txt file\nas there are library names in the libraryNames.txt File\n")
            self.errorCnt +=1
        print ("libraries are:", self.libraries)
        print ("indexes are:", self.indexes)




    # -------------------------------------------------------------------------------------------
    # updateIndex
    # gets the library artifact URL
    # goes to library artifacts and updates the  index
    # output:

    def updateIndex(self, libraryName):
        url = self.myUDF.setURLartifact(libraryName, self.apiDevServer)
        if url:
            libURL = self.myUDF.connectToClarityAPIartifacts(url,'lib')
            print("Library Artifact URL = ", libURL)
            artifacts = self.myUDF.getArtifactUDFs(libURL)
            print ("old index for ",artifacts["Library Name"],": ", artifacts["Index Name"])
            newIndex = self.indexes[self.indexIter]
            self.indexIter += 1
            self.myUDF.updateArtifactUDF(libURL, "Index Name", newIndex)
            artifacts = self.myUDF.getArtifactUDFs(libURL)
            print("new index for ", artifacts["Library Name"], ": ", artifacts["Index Name"])



# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# main
robot = AutoTester_IndexUpdater()
robot.getInput()  #from files

numOfLibraries = 0

if robot.errorCnt == 0:
    for library in robot.libraries:
        print ("Library: ",library)
        robot.updateIndex(library)
        numOfLibraries +=1

    print("go to: https://clarity-dev01.jgi-psf.org/clarity/queue/53 and look for these libraries:")
    print(robot.libraries)


print("\n---Number of Errors Found = " + str(robot.errorCnt) + " ---")
print ("updated ", numOfLibraries, " libraries ")



