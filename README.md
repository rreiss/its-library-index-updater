# ITS Library Index Updater

This script will make testing of pooling easier and faster.
It takes two files as input.

file #1 = C:\clarity_jar\libraryNames.txt
  this is the list of library names (must be all cap) that you would
  like the indexes updated.

file #2 = C:\clarity_jar\indexes.txt
  this is the list of index names (must be copied from DB) that will
  be used to update the libraries.

process:  For each library in the file, the script will update the
  clarity library's index UDF with the new value read from
  the index file.

to prepare to run this script:
#1 go to GLS.artifact_index table and select/copy indexes from
   Sequence_ID column (make sure that the index is active (=1))
#2 paste the indexes into a text file C:\clarity_jar\indexes.txt

#3 go to Clarity and select libraries from the LP Pool Creation
   queue.
   An easy way to select them is to put them in the ice bucket.
   View ice bucket and hit print button. This will give you a
   spread sheet of the libraries and you can copy and paste
   to the file C:\clarity_jar\libraryNames.txt

#4 once you have the files in place, run the script by right
   clicking on  "autTester_IndexUpdater.py" and select Run from
   the menu.